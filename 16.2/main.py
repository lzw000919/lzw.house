"""
   练习16 第二问
"""
import csv
import matplotlib.pyplot as plt
from pylab import mpl

x_list = list()
y_list = list()
with open('D:\\code\\14\\数据.csv', 'r') as f:
    read = csv.reader(f)
    for row in read:
        if row[2] != '用户等级' and row[2] != '':
            y_list.append(int(row[2]))
for i in range(len(y_list)):
    x_list.append(int(i + 1))

mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
plt.figure(figsize=(10, 10), dpi=100)
plt.scatter(x_list, y_list, s=5, color=(0., 0.5, 0.))
plt.title("用户等级散点图")
plt.grid(True, linestyle='-', alpha=0.5)
plt.savefig("用户等级散点图.png")
plt.show()
