"""
   练习9
"""
import string
from collections import Counter

with open('D:\code\9\The Old Man and the Sea.txt', 'r', encoding='utf-8', errors='ignore') as ReadFile:
    ReadData = ReadFile.readlines()
msg = ""
for line in ReadData:
    msg = msg + line
msg = msg.lower()
new_msg = ''.join(c for c in msg if c not in string.punctuation and c != '\"')
l = new_msg.split()
result = Counter(l).most_common(10)
print("出现次数最多的10个单词：")
print("单词 \t 出现次数")
for i in range(10):
    print(result[i][0], end="\t\t  ")
    print(result[i][1])
