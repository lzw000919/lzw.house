"""
   练习2
"""


def s(sa, sb):
    if sa <= 0 or sb <= 0:
        return -1
    if (sa + sb) * (sa + sb) <= sa * sa + sb * sb:
        return -1
    return (sa * sb) / 2


a = float(input("请输入直角三角形的第一条直角边："))
b = float(input("请输入直角三角形的第二条直角边："))
print(s(a, b))
