"""
   练习4
"""
import turtle

t = turtle.Turtle(shape="turtle")
i = 1
t.penup()
t.goto(0, 100)
t.pendown()
t.begin_fill()
t.right(72)
t.fd(100)
while i <= 4:
    t.left(72)
    t.fd(100)
    t.right(180 - 36)
    t.fd(100)
    i = i + 1
t.left(72)
t.fd(100)
t.color('yellow')
t.end_fill()
t.hideturtle()
turtle.mainloop()
