"""
   练习16 第一问
"""
import csv
import matplotlib.pyplot as plt
from pylab import mpl

level_list = list()
x_list = list()
y_list = list()
with open('D:\\code\\14\\数据.csv', 'r') as f:
    read = csv.reader(f)
    for row in read:
        if row[2] != '用户等级' and row[2] != '':
            level_list.append(int(row[2]))
for i in range(min(level_list), max(level_list) + 1):
    x_list.append(str('等级为{}'.format(i)))
    y_list.append(0)
for i in range(len(level_list)):
    y_list[level_list[i]] = y_list[level_list[i]] + 1
# 设置matplotlib对于中文的支持
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(20, 12), dpi=100)
axes[0].pie(y_list, labels=x_list, autopct="%.2f%%")  # explode用 于设定扇区距离中心的远近
axes[1].bar(x_list, y_list, width=0.1)
axes[0].set_title('用户等级分布饼状图', fontsize=20)
axes[1].set_title('用户等级分布柱状图', fontsize=20)
plt.savefig('用户等级分布.png')
plt.show()
