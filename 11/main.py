"""
   练习11
"""
import random

n = int(input("需要多少注："))
l = list()
for i in range(n):
    red = list(range(1, 34))
    blue = list(range(1, 17))
    red_list = list()
    blue_list = list()
    for j in range(6):
        a = random.choice(red)
        red_list.append(a)
        del red[red.index(a)]
    b = random.choice(blue)
    blue_list.append(b)
    del blue[blue.index(b)]
    l.append(red_list)
    l.append(blue_list)
selection = input("请输入你的选择，保存文件还是直接输出？")
if selection == "保存文件":
    with open('D:\code\double_color_ball.txt', 'a') as file:
        for i in range(0, 6, 2):
            file.write(repr(l[i]))
            file.write(repr(l[i+1])+'\n')
else:
    print("\t\t红球\t\t\t\t  蓝球")
    for i in range(0, 6, 2):
        print(l[i], end="\t")
        print(l[i + 1])
