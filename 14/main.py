"""
   练习14
"""
import time

import requests

headers = {
    'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36 Edg/91.0.864.71",
}
with open("D:\\code\\14\\数据.csv", 'w')as f:
    f.write(','.join(['用户编号', '用户名', '用户等级', '用户是否存在\n']))
    for i in range(1, 501):
        time.sleep(1)
        url = "https://api.bilibili.com/x/space/acc/info?mid={}&jsonp=jsonp".format(i)
        r = requests.get(url=url, headers=headers)
        result = r.json()
        if result['code'] == 0:
            f.write(','.join([
                str(result['data']['mid']),
                str(result['data']['name']),
                str(result['data']['level']),
                str('用户存在') + '\n',
            ]))
        else:
            f.write(','.join([
                str(''),
                str(''),
                str(''),
                str('用户已注销') + '\n',
            ]))
print("爬取完成")

