"""
   练习1
"""
a = float(input("请输入直角三角形的第一条直角边："))
b = float(input("请输入直角三角形的第二条直角边："))
if a <= 0 or b <= 0:
    print("错误")
if (a + b) * (a + b) <= a * a + b * b:  # 两边之和要大于第三边
    print("错误")
s = (a * b) / 2
print("直角三角形的面积为", s)
