"""
   练习18
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pylab import mpl

data = pd.read_csv('D:/code/18/IMDB-Movie-Data.csv', encoding='gbk')
print('电影的评分平均分为：', data['Rating'].mean())

count1 = pd.crosstab(data['Rating'], data['Runtime (Minutes)'])
count2 = pd.crosstab(data['Runtime (Minutes)'], data['Rating'], )
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
count1.plot(kind='hist')
plt.title('Rating分布直方图')
count2.plot(kind='hist')
plt.title('Runtime分布直方图')
plt.show()

genre = set()
for row in data['Genre']:
    for i in row.split(','):
        genre.add(i)
genre_list = list(genre)
title_list = list()
for row in data['Title']:
    title_list.append(row)
values = np.zeros(shape=(1000, len(genre_list)))
genre_df = pd.DataFrame(values, index=title_list, columns=genre_list, dtype=int)

k = 0
for row in data['Genre']:
    for i in row.split(','):
        genre_df[str(i)][str(data['Title'][k])] = 1
    k = k + 1
print("电影的分类情况汇总：")
print("电影类型\t\t数量")
print(genre_df.sum())
