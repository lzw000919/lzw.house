"""
   练习12
"""
import random
import time
import datetime

red = list(range(1, 34))
blue = list(range(1, 17))
red_list = list()
blue_list = list()
selection = int(input("是否进行开奖：1.是\t2.否"))
if selection == 1:
    for i in range(6):
        red_temp = random.choice(red)
        print("红", red_temp, end=" ")
        red_list.append(red_temp)
        del red[red.index(red_temp)]
        if i <= 4:
            time.sleep(3)
    blue_temp = random.choice(blue)
    time.sleep(5)
    print("蓝", blue_temp)
    blue_list.append(blue_temp)
    with open('D:\code\information.txt', 'a') as file:
        file.write("开奖信息：")
        file.write('红：' + repr(red_list))
        file.write('蓝:' + repr(blue_list) + '\n')
        file.write('\t\t\t\t\t\t\t\t\t\t\t开奖日期：' + str(datetime.date.today()) + '\n')
    print("开奖信息已经写入文件")
else:
    exit(0)
