"""
   练习13
"""
import os
import shutil
import random

suffx_list = ['.jpg', '.mp4', '.txt',
              '.docx', '.pptx', '.xlsx',
              '.png', '.py', '.c', '.cpp',
              '.exe', '.mp3', '.gif',
              '.o', '.px', '.q', '.java'
              ]
""""  混乱文件生成
os.makedirs('D:\code\杂乱的文件目录', exist_ok=True)
for i in range(10000):
    filepath = os.path.join('D:\code\杂乱的文件目录',
                            str(random.randint(0, 99999999999)) + suffx_list[random.randrange(0, len(suffx_list))])
    with open(filepath, 'w') as f:
        pass
"""

# 文件归档操作
path = 'D:\\code\\13\\杂乱的文件目录'
dir = os.listdir(path)
for i in dir:
    if i.rfind('.') != -1:
        full_path = os.path.join(path, i)
        folder_name = i.split('.')[-1]
        print(os.path.isfile(full_path))
        os.makedirs(folder_name, exist_ok=True)
        shutil.move(full_path, folder_name)
# 文件整理操作（如果遇到重名文件，则删除）
for index in range(len(suffx_list)):
    l = list()
    l.append('0')
    path_list = os.path.join('D:\\code\\13', suffx_list[index].split('.')[-1])
    for f in os.listdir(path_list):
        a = f.split('.')
        for k in range(len(l)):
            if l[k] == a[0]:
                p = 'D:\\code\\13\\' + suffx_list[index].split('.')[-1] + '\\' + f
                os.remove(p)
