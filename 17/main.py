"""
   练习17
"""
import numpy as np
import pandas as pd
from pandas import IntervalIndex

data = pd.read_csv('D:/code/14/数据.csv', encoding='gbk')
level = data['用户等级']
bins = [-1, 1, 3, 5, 6]
p_counts = pd.cut(level, bins, labels=['新用户', '普通用户', '老用户', '骨灰级用户'])
dummies = pd.get_dummies(p_counts)
print(dummies)
print('数量统计:\n',dummies.sum())
