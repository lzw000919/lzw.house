"""
   练习5
"""
import turtle

a = float(input("请输入等边三角形边长："))
r = float(input("请输入圆的半径："))
if a < 100 or a > 200:
    print("错误")
if r < 100 or r > 200:
    print("错误")
t = turtle.Turtle(shape="turtle")
t.penup()
t.goto(-200,0)
t.pendown()
t.right(60)
t.fd(a)
t.right(120)
t.fd(a)
t.right(120)
t.fd(a)
t.right(60)
t.penup()
t.goto(200, -r)
t.pendown()
t.circle(r)
t.hideturtle()
turtle.mainloop()
