"""
   获取最新的疫情数据，并实时的创建csv文件，将数据存入
"""
import json
import time
import requests
import os

url = 'http://view.inews.qq.com/g2/getOnsInfo?name=disease_h5&callback=&_=%d' % int(time.time() * 1000)
data = json.loads(requests.get(url=url).json()['data'])

# 统计省份的信息
sum = data['areaTree'][0]['children']

# 解析数据
# 累计确诊病例数据
total_confirm_data = {}
for item in sum:
    if item['name'] not in total_confirm_data:
        total_confirm_data.update({item['name']: 0})
    for city in item['children']:
        total_confirm_data[item['name']] += int(city['total']['confirm'])

# 现有确诊病例数据
now_confirm_data = {}
for item in sum:
    if item['name'] not in now_confirm_data:
        now_confirm_data.update({item['name']: 0})
    for city in item['children']:
        now_confirm_data[item['name']] += int(city['total']['nowConfirm'])

# 死亡人数数据
dead_data = {}
for item in sum:
    if item['name'] not in dead_data:
        dead_data.update({item['name']: 0})
    for city in item['children']:
        dead_data[item['name']] += int(city['total']['dead'])

# 治愈人数数据
heal_data = {}
for item in sum:
    if item['name'] not in heal_data:
        heal_data.update({item['name']: 0})
    for city in item['children']:
        heal_data[item['name']] += int(city['total']['heal'])

# 新增确诊数据
new_confirm_data = {}
for item in sum:
    if item['name'] not in new_confirm_data:
        new_confirm_data.update({item['name']: 0})
    for city in item['children']:
        new_confirm_data[item['name']] += int(city['today']['confirm'])

names = list(total_confirm_data.keys())
num1 = list(total_confirm_data.values())  # 累计确诊
num2 = list(new_confirm_data.values())  # 新增确诊
num3 = list(now_confirm_data.values())  # 现有确诊
num4 = list(dead_data.values())  # 累计死亡
num5 = list(heal_data.values())  # 累计治愈


print("地区\t\t\t累计确诊人数\t\t\t新增确诊人数\t\t\t现有确诊人数\t\t\t累计死亡人数\t\t\t累计治愈人数")
for i in range(34):
    print("{:<4s}".format(names[i]), end='\t\t')
    print("{:<6d}".format(num1[i]), end='\t\t\t\t')
    print("{:<6d}".format(num2[i]), end='\t\t\t\t')
    print("{:<6d}".format(num3[i]), end='\t\t\t\t')
    print("{:<6d}".format(num4[i]), end='\t\t\t\t')
    print("{:<6d}".format(num5[i]))

# 获取当前的日期
date_f = time.strftime("%Y-%m-%d") + "-国内疫情数据报告.csv"
if os.path.exists(date_f):
    print("文件已生成")
else:
    with open(date_f, 'w', encoding='utf-8') as f:
        f.write(','.join(['地区', '累计确诊人数', '新增确诊人数', '现有确诊人数', '累计死亡人数', '累计治愈人数\n']))
        for i in range(34):
            f.write(','.join([names[i], str(num1[i]), str(num2[i]), str(num3[i]), str(num4[i]), str(num5[i]) + '\n']))
    print("数据已写入文件")

