"""
   基于最新疫情数据生成的csv文件，来绘制实时的疫情信息统计图
"""
import time
import matplotlib.pyplot as plt
import pandas as pd
from pylab import mpl

# 读取文件数据
n = time.strftime("%Y-%m-%d") + "-国内疫情数据报告.csv"
p = time.strftime("%Y-%m-%d") + "-各省市疫情分布.png"
data = pd.read_csv(n)

# 整理数据
names = list(data['地区'])
new_confirm_data = list(data['新增确诊人数'])
now_confirm_data = list(data['现有确诊人数'])
dead_data = list(data['累计死亡人数'])
heal_data = list(data['累计治愈人数'])

plt.figure('各省市疫情分布',figsize=(20, 8), dpi=100)
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False

# 绘制现有确诊数据
p1 = plt.subplot(221)
plt.text(15, 2200, "实时确诊人数、新增确诊人数、死亡人数、治愈人数对比图", size=18)
plt.bar(names, now_confirm_data, width=0.3, color='red')
plt.ylabel("现有确诊人数", rotation=90)
plt.xticks(names, rotation=-60, size=8)
for a, b in zip(names, now_confirm_data):
    plt.text(a, b, b, ha='center', va='bottom', size=6)
plt.sca(p1)

# 绘制新增确诊数据
p2 = plt.subplot(222)
plt.bar(names, new_confirm_data, width=0.3, color='blue')
plt.ylabel("新增确诊人数", rotation=90)
plt.xticks(names, rotation=-60, size=8)
for a, b in zip(names, new_confirm_data):
    plt.text(a, b, b, ha='center', va='bottom', size=6)
plt.sca(p2)

# 绘制累计死亡数据
p3 = plt.subplot(223)
plt.bar(names, dead_data, width=0.3, color='grey')
plt.xlabel("地区")
plt.ylabel("累计死亡人数", rotation=90)
plt.xticks(names, rotation=-60, size=8)
for a, b in zip(names, dead_data):
    plt.text(a, b, b, ha='center', va='bottom', size=6)
plt.sca(p3)

# 绘制累计治愈数据
p4 = plt.subplot(224)
plt.bar(names, heal_data, width=0.3, color='green')
plt.xlabel("地区")
plt.ylabel("累计治愈人数", rotation=90)
plt.xticks(names, rotation=-60, size=8)
for a, b in zip(names, heal_data):
    plt.text(a, b, b, ha='center', va='bottom', size=6)
plt.sca(p4)
plt.savefig(p)
plt.show()
