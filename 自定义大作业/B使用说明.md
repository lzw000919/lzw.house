## **使用说明**
#### **1.windows ×64位操作系统，python=3.8版本环境，第三方库：matplotlib,requests,json,pandas,datetime**
#### **2.操作：**
###首先运行Get_Data.py脚本，进行实时数据的读取，并存入为当日创建的csv文件中，csv文件名为日期+疫情数据报告
![img.png](img.png)
![img_1.png](img_1.png)


##接着运行Statistical_Chart.py脚本，来读取当日的csv文件，并绘制各省市的数据对比图
![img_2.png](img_2.png)


##最后运行Rencent_Statistical_Plot.py脚本，绘制近期的国内总疫情趋势曲线图
![img_3.png](img_3.png)