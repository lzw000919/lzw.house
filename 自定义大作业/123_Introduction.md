###**基于腾讯API接口实现实时疫情监控系统**

#####**整体结构共分为三个部分：**
* 获取并保存实时数据（Get_data.py)
* 根据所得数据绘制各省市统计对比图(Statistical_Chart.py)
* 绘制近期国内疫情的变化趋势(Recent_Statistical_plot.py)