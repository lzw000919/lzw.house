"""
   练习10
"""
import math


class RangeError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        print("不符合三角形规则,", end="")


class ZeroError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        print("数据必须大于0,", end="")


flag = 1
while flag == 1:
    try:
        a = float(input("请输入直角三角形的第一条直角边："))
        b = float(input("请输入直角三角形的第二条直角边："))
        if a <= 0 or b <= 0:
            raise ZeroError(1)
        if (a + b) <= math.sqrt(a * a + b * b) or a >= math.sqrt(a * a + b * b) or b >= math.sqrt(a * a + b * b):
            raise RangeError(1)
    except ValueError as e:
        print("输入非法数据，必须输入数字,重新输入")
    except ZeroError as e:
        print("输入非法数据,重新输入", e.__str__())
    except RangeError as e:
        print("输入非法数据,重新输入", e.__str__())
    else:
        s = (a * b) / 2
        print("直角三角形的面积为", s)
        flag = 0
print("程序结束！")
