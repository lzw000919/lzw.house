"""
   练习3
"""
n = int(input("请输入一个数字："))
if n > 10 or n < 1:
    print("输入有误！请输入1-10之间的数字。")
i = 1
j = 1
k = 1
while i <= n:
    while j <= n - i:
        print(" ", end="")
        j = j + 1
    while k <= (2 * i - 1):
        print("=", end="")
        k = k + 1
    print(end="\n")
    k = 1
    j = 1
    i = i + 1
