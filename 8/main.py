"""
   练习8
"""


def coding(string):  # 加密程序
    l = list(string)
    for i in range(len(l)):
        print(ord(l[i]), end=" ")
    print(end="\n")


def encoding(l):  # 解密程序
    for i in range(len(l)):
        print(chr(l[i]), end="")


s = input("输入一串文字:")
t = [25105, 29233, 23398, 20064]
print("加密后为：")
coding(s)
print("解密列表t为：")
encoding(t)
