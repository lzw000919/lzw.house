"""
   练习15
"""


class Pokemon:
    def __init__(self, name):
        self.name = name

    def run(self):
        print(self.name, "已生成")


class Water_Pokemon(Pokemon):
    def skill(self):
        print("使用通用技能：水枪")


class Grass_Pokemon(Pokemon):
    def skill(self):
        print("使用通用技能：飞叶快刀")


class Fire_Pokemon(Pokemon):
    def skill(self):
        print("使用通用技能：喷火")


class Create_Pokemon(Water_Pokemon, Grass_Pokemon, Fire_Pokemon):
    pass




A = Water_Pokemon("杰尼龟")
A.run()
A.skill()

B = Grass_Pokemon("妙蛙种子")
B.run()
B.skill()

C = Fire_Pokemon("小火龙")
C.run()
C.skill()

D = Create_Pokemon("自定义生物")
D.run()
D.skill()

